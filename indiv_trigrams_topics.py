from elasticsearch import Elasticsearch
import configparser
import create_trigrams_keywords
# Gets the trigrams of a given "freq" in a given "text"

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

def get_author(search_after, es) :
    query = {
        "size" : 1000,
        "query" : {"match_all" : {}}
    }
    return es.search(index=index_author, doc_type=doc_type_author, body=query,scroll="20m")

def nb_raweb_author() :
    return es.search(index=index_author, doc_type=doc_type_author, body={"query" : {"match_all" : {}}})["hits"]["total"]

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip")+":"+config.get("elasticsearch", "port"))
index_author = config.get("elasticsearch", "index_author")
doc_type_author = config.get("elasticsearch", "doc_type_author")
index_all_pub = config.get("elasticsearch", "index_all_pub")
doc_type_all_pub = config.get("elasticsearch", "doc_type_all_pub")

total_raweb_author = nb_raweb_author()
range_author = int(total_raweb_author/1000) + 1

for i in range(0, range_author) :
    if i == 0 :
        authors = get_author(i, es)
    else :
        authors = es.scroll(scroll_id=scroll_id, scroll="30m")
    scroll_id = authors["_scroll_id"]
    completeText = ""
    for author in authors["hits"]["hits"] :
        completeText = ""
        if "pubs" in author["_source"] :
            for publication in author["_source"]["pubs"]:
                title_fr = publication["title_fr"]
                title_en = publication["title_en"]
                abstract_fr = ""
                abstract_en = ""
                if "abstract_fr" in publication :
                    abstract_fr = publication["abstract_fr"]
                if "abstract_en" in publication :
                    abstract_en = publication["abstract_en"]
                completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
                completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
            maintopic_data = {}
            data_list = []
            if completeText != "" :
                data_list = create_trigrams_keywords.get_trigrams(es, completeText, index_all_pub, doc_type_all_pub)
            maintopic_data["main_topics"] = data_list if data_list else ["empty"]
            es.update(index=index_author, doc_type=doc_type_author, id=author["_id"] ,body={ "doc" : maintopic_data})
